# HighScooler - ochłoń przed rekrutacją

![logo](https://gitlab.com/viodine/highschooler/raw/master/app/assets/images/highscooler_logo.png)

**Aplikacja internetowa wspomagająca wybór studiów oraz obliczanie punktów rekrutacyjnych.**

_Celem pracy jest stworzenie aplikacji internetowej do przeliczania wyników egzaminów maturalnych na wskazniki rekrutacyjne odpowiednie dla kierunków wybranych przez uzytkownika. Interfejs pozwala uzytkownikom na m. in. przegladanie kierunków dostepnych na uczelniach w calej Polsce, sprawdzenie prógów punktowych z poprzednich lat oraz dostep do harmonogramów rekrutacji._

Użyte technologie:

- Ruby / RoR

- RSpec / Faker

- PostgreSQL / ActiveRecord

- HAML / SASS

- Bulma

- VueJS / jQuery

**Planowany deploy wersji alpha: 10.2019**
