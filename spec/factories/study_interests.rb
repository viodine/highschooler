# frozen_string_literal: true

FactoryBot.define do
  factory :study_interest do
    user
    field_of_study
  end
end
