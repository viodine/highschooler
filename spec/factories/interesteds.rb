# frozen_string_literal: true

FactoryBot.define do
  factory :interested do
    field_detail
    user
  end
end
