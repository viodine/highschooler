# frozen_string_literal: true

module Users
  class RegistrationsController < Devise::RegistrationsController
    before_action :configure_sign_up_params, only: [:create]

    # GET /resource/sign_up
    def new
      @user = User.new
      @user.matura_results.build
      @user.study_interests.build
    end

    # POST /resource
    def create
      build_resource(sign_up_params)
      resource.save
      yield resource if block_given?
      if resource.persisted?
        if resource.active_for_authentication?
          set_flash_message! :notice, :signed_up
          sign_up(resource_name, resource)
        else
          set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
          expire_data_after_sign_in!
        end
      else
        clean_up_passwords resource
        set_minimum_password_length
        respond_with resource
      end
    end

    protected

    # If you have extra params to permit, append them to the sanitizer.
    def configure_sign_up_params
      devise_parameter_sanitizer.permit(:sign_up, keys:
        [:username, :email, :password, :account_type,
        :gender, :date_of_birth, :city, :high_school_id, :academy_id, :field_of_study_id, :date_of_matura, matura_results_attributes: [:id, :user_id, :matura_subject_id, :level, :result],
        study_interests_attributes: [:id, :user_id, :field_of_study_id]])
    end
  end
end
